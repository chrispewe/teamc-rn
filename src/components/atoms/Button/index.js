import React from 'react'
import { StyleSheet, Text, } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { colors } from '../../../utils'
import Icon from 'react-native-vector-icons/Ionicons'

const Button = ({ title, onPress, type, white }) => {

    if (type === "default") {
        return (<TouchableOpacity
            style={
                styles.componentDefault(white)
            }
            onPress={onPress}>
            <Text
                style={styles.title(white)}>
                {title}
            </Text>
        </TouchableOpacity>)
    }
    if (type === "Add to basket") {
        return (<TouchableOpacity
            style={styles.componentloginSocmed}
            onPress={onPress}>
            <Icon style={{marginRight:10}}name='ios-home' size={20} color={'#2E3E5C'} />
            <Text
                style={styles.title(white)}>
                {title}
            </Text>
        </TouchableOpacity>)
    }
}

export default Button


const styles = {
    componentDefault: (white)=>{
        return{
        height: 56,
        backgroundColor: white?'white':colors.primary,
        borderRadius: 10,
        justifyContent: 'center',
    }
    },
    componentloginSocmed: {
        flexDirection:'row',
        width: 327,
        height: 56,
        backgroundColor: colors.primary,
        borderRadius: 32,
        alignItems:'center',
        justifyContent:'center'
    },
    title: (white)=>{
        return{
        fontSize: 15,
        fontWeight: 'bold',
        color: white?'#9FA5C0':'white',
        textAlign: 'center',
    }
    }

}