import SplashScreen from "./SplashScreen";
import LoginPage from "./LoginPage";
import RegisterPage from "./RegisterPage";
import HomePage from "./HomePage";
import ProfilePage from "./ProfilePage";
import HomePageDetail from "./HomePageDetail";
import HomePageAddReview from "./HomePageAddReview";
import UserReviewPage from "./UserReviewPage";
import ReviewsPageEditReview from "./ReviewsPageEditReview";


export{SplashScreen,LoginPage,RegisterPage,HomePage,ProfilePage,HomePageDetail,HomePageAddReview,UserReviewPage,ReviewsPageEditReview};


